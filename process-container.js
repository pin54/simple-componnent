import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';
import './process-element.js';

class ProcessContainer extends PolymerElement {

    static get template() {
        return html `
            <dom-repeat items="{{processes}}" as="process">
                <template>
                    <process-element id={{process.id}} required="{{process.required}}" 
                        parallel="{{process.parallel}}" on-change="_handleChange">
                    </process-element>
                </template>                
            </dom-repeat>

            <paper-button on-click="_handleClick">Sprawdź</paper-button>            
        `;
    }

    static get properties() {
        return {
            processes: {
                type: Array,
            }
        }
    }

    _handleChange(e){
        //usunąłem całą funkcjonalność
        this._handleClick();
    }

    _handleClick() {
        var i;
        for(i = 0; i < this.processes.length; ++i){
            console.log("ProcessContainer: ProcessElement id: " + this.processes[i].id + ", required " +
                this.processes[i].required + ", parallel " + this.processes[i].parallel);
        }
    }

    constructor(){
        super();
        this.processes = [
            {id: 1, required: true, parallel: false},
            {id: 2, required: true, parallel: true}
        ];
    }
}

customElements.define('process-container', ProcessContainer);
