import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-checkbox/paper-checkbox.js';
import '@polymer/paper-button/paper-button.js';

class ProcessElement extends PolymerElement {
    static get template() {
        return html `
            <div class="process-box">
                <paper-checkbox name="required" checked={{required}} class="blue" on-click="_handleClick">Wymagany</paper-checkbox>
                <paper-checkbox name="parallel" checked={{parallel}} class="blue" on-click="_handleClick">Równoległy</paper-checkbox>                    
            </div>
        `;
    }

    static get properties() {
        return {
            properties: {
                id: Number,
                required: Boolean,
                parallel: Boolean,
                order: Number,
                notify: true
            }
        }
    }

    _handleClick(e){
        console.log("Process-element id: " + this.id + ", required: " + this.required + ", parallel: " + this.parallel);
    }

    constructor() {
        super();
        //this.required = true;
    }
}

customElements.define('process-element', ProcessElement);